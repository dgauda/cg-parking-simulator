package com.cap.simulatorservices.parkingmanagement.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface ParkingLot extends ApplicationEntity {

  /**
   * @return addressId
   */

  public String getAddress();

  /**
   * @param address setter for address attribute
   */

  public void setAddress(String address);

  /**
   * @return latitudeId
   */

  public Double getLatitude();

  /**
   * @param latitude setter for latitude attribute
   */

  public void setLatitude(Double latitude);

  /**
   * @return longitudeId
   */

  public Double getLongitude();

  /**
   * @param longitude setter for longitude attribute
   */

  public void setLongitude(Double longitude);

  /**
   * @return totalNoOfSlotsId
   */

  public Long getTotalNoOfSlots();

  /**
   * @param totalNoOfSlots setter for totalNoOfSlots attribute
   */

  public void setTotalNoOfSlots(Long totalNoOfSlots);

  /**
   * @return availableNoOfSlotsId
   */

  public Long getAvailableNoOfSlots();

  /**
   * @param availableNoOfSlots setter for availableNoOfSlots attribute
   */

  public void setAvailableNoOfSlots(Long availableNoOfSlots);

  /**
   * @return operatingcompanyId
   */

  public String getOperatingcompany();

  /**
   * @param operatingcompany setter for operatingcompany attribute
   */

  public void setOperatingcompany(String operatingcompany);

}
