package com.cap.simulatorservices.parkingmanagement.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface ParkingSlot extends ApplicationEntity {

  /**
   * getter for parkingLotId attribute
   * 
   * @return parkingLotId
   */

  public Long getParkingLotId();

  /**
   * @param parkingLot setter for parkingLot attribute
   */

  public void setParkingLotId(Long parkingLotId);

  /**
   * @return slotNumberId
   */

  public Long getSlotNumber();

  /**
   * @param slotNumber setter for slotNumber attribute
   */

  public void setSlotNumber(Long slotNumber);

  /**
   * getter for statusId attribute
   * 
   * @return statusId
   */

  public Long getStatusId();

  /**
   * @param status setter for status attribute
   */

  public void setStatusId(Long statusId);

}
