package com.cap.simulatorservices.parkingmanagement.common.api;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface Reservation extends ApplicationEntity {

  /**
   * @return itineraryIdId
   */

  public Long getItineraryId();

  /**
   * @param itineraryId setter for itineraryId attribute
   */

  public void setItineraryId(Long itineraryId);

  /**
   * @return startTimestampId
   */

  public Timestamp getStartTimestamp();

  /**
   * @param startTimestamp setter for startTimestamp attribute
   */

  public void setStartTimestamp(Timestamp startTimestamp);

  /**
   * @return endTimestampId
   */

  public Timestamp getEndTimestamp();

  /**
   * @param endTimestamp setter for endTimestamp attribute
   */

  public void setEndTimestamp(Timestamp endTimestamp);

  /**
   * getter for parkingLotId attribute
   *
   * @return parkingLotId
   */

  public Long getParkingLotId();

  /**
   * @param parkingLot setter for parkingLot attribute
   */

  public void setParkingLotId(Long parkingLotId);

  /**
   * getter for parkingSlotId attribute
   *
   * @return parkingSlotId
   */

  public Long getParkingSlotId();

  /**
   * @param parkingSlot setter for parkingSlot attribute
   */

  public void setParkingSlotId(Long parkingSlotId);

}
