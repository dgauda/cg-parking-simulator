package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import java.util.Set;

import com.devonfw.module.basic.common.api.to.AbstractCto;

/**
 * Composite transport object of ParkingLot
 */
public class ParkingLotCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ParkingLotEto parkingLot;

  private Set<ParkingSlotEto> parkingSlots;

  public ParkingLotEto getParkingLot() {

    return parkingLot;
  }

  public void setParkingLot(ParkingLotEto parkingLot) {

    this.parkingLot = parkingLot;
  }

  public Set<ParkingSlotEto> getParkingSlots() {

    return parkingSlots;
  }

  public void setParkingSlots(Set<ParkingSlotEto> parkingSlots) {

    this.parkingSlots = parkingSlots;
  }

}
