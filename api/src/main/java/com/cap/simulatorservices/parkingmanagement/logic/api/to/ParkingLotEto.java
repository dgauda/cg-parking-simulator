package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.cap.simulatorservices.parkingmanagement.common.api.ParkingLot;
import com.devonfw.module.basic.common.api.to.AbstractEto;

/**
 * Entity transport object of ParkingLot
 */
public class ParkingLotEto extends AbstractEto implements ParkingLot {

  private static final long serialVersionUID = 1L;

  private String address;

  private Double latitude;

  private Double longitude;

  private Long totalNoOfSlots;

  private Long availableNoOfSlots;

  private String operatingcompany;

  @Override
  public String getAddress() {

    return address;
  }

  @Override
  public void setAddress(String address) {

    this.address = address;
  }

  @Override
  public Double getLatitude() {

    return latitude;
  }

  @Override
  public void setLatitude(Double latitude) {

    this.latitude = latitude;
  }

  @Override
  public Double getLongitude() {

    return longitude;
  }

  @Override
  public void setLongitude(Double longitude) {

    this.longitude = longitude;
  }

  @Override
  public Long getTotalNoOfSlots() {

    return totalNoOfSlots;
  }

  @Override
  public void setTotalNoOfSlots(Long totalNoOfSlots) {

    this.totalNoOfSlots = totalNoOfSlots;
  }

  @Override
  public Long getAvailableNoOfSlots() {

    return availableNoOfSlots;
  }

  @Override
  public void setAvailableNoOfSlots(Long availableNoOfSlots) {

    this.availableNoOfSlots = availableNoOfSlots;
  }

  @Override
  public String getOperatingcompany() {

    return operatingcompany;
  }

  @Override
  public void setOperatingcompany(String operatingcompany) {

    this.operatingcompany = operatingcompany;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.address == null) ? 0 : this.address.hashCode());
    result = prime * result + ((this.latitude == null) ? 0 : this.latitude.hashCode());
    result = prime * result + ((this.longitude == null) ? 0 : this.longitude.hashCode());
    result = prime * result + ((this.totalNoOfSlots == null) ? 0 : this.totalNoOfSlots.hashCode());
    result = prime * result + ((this.availableNoOfSlots == null) ? 0 : this.availableNoOfSlots.hashCode());
    result = prime * result + ((this.operatingcompany == null) ? 0 : this.operatingcompany.hashCode());

    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    ParkingLotEto other = (ParkingLotEto) obj;
    if (this.address == null) {
      if (other.address != null) {
        return false;
      }
    } else if (!this.address.equals(other.address)) {
      return false;
    }
    if (this.latitude == null) {
      if (other.latitude != null) {
        return false;
      }
    } else if (!this.latitude.equals(other.latitude)) {
      return false;
    }
    if (this.longitude == null) {
      if (other.longitude != null) {
        return false;
      }
    } else if (!this.longitude.equals(other.longitude)) {
      return false;
    }
    if (this.totalNoOfSlots == null) {
      if (other.totalNoOfSlots != null) {
        return false;
      }
    } else if (!this.totalNoOfSlots.equals(other.totalNoOfSlots)) {
      return false;
    }
    if (this.availableNoOfSlots == null) {
      if (other.availableNoOfSlots != null) {
        return false;
      }
    } else if (!this.availableNoOfSlots.equals(other.availableNoOfSlots)) {
      return false;
    }
    if (this.operatingcompany == null) {
      if (other.operatingcompany != null) {
        return false;
      }
    } else if (!this.operatingcompany.equals(other.operatingcompany)) {
      return false;
    }

    return true;
  }
}
