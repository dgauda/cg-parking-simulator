package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractSearchCriteriaTo;
import com.devonfw.module.basic.common.api.query.StringSearchConfigTo;

/**
 * {@link SearchCriteriaTo} to find instances of
 * {@link com.cap.simulatorservices.parkingmanagement.common.api.ParkingLot}s.
 */
public class ParkingLotSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String address;

  private Double latitude;

  private Double longitude;

  private Long totalNoOfSlots;

  private Long availableNoOfSlots;

  private String operatingcompany;

  private StringSearchConfigTo addressOption;

  private StringSearchConfigTo operatingcompanyOption;

  /**
   * @return addressId
   */

  public String getAddress() {

    return address;
  }

  /**
   * @param address setter for address attribute
   */

  public void setAddress(String address) {

    this.address = address;
  }

  /**
   * @return latitudeId
   */

  public Double getLatitude() {

    return latitude;
  }

  /**
   * @param latitude setter for latitude attribute
   */

  public void setLatitude(Double latitude) {

    this.latitude = latitude;
  }

  /**
   * @return longitudeId
   */

  public Double getLongitude() {

    return longitude;
  }

  /**
   * @param longitude setter for longitude attribute
   */

  public void setLongitude(Double longitude) {

    this.longitude = longitude;
  }

  /**
   * @return totalNoOfSlotsId
   */

  public Long getTotalNoOfSlots() {

    return totalNoOfSlots;
  }

  /**
   * @param totalNoOfSlots setter for totalNoOfSlots attribute
   */

  public void setTotalNoOfSlots(Long totalNoOfSlots) {

    this.totalNoOfSlots = totalNoOfSlots;
  }

  /**
   * @return availableNoOfSlotsId
   */

  public Long getAvailableNoOfSlots() {

    return availableNoOfSlots;
  }

  /**
   * @param availableNoOfSlots setter for availableNoOfSlots attribute
   */

  public void setAvailableNoOfSlots(Long availableNoOfSlots) {

    this.availableNoOfSlots = availableNoOfSlots;
  }

  /**
   * @return operatingcompanyId
   */

  public String getOperatingcompany() {

    return operatingcompany;
  }

  /**
   * @param operatingcompany setter for operatingcompany attribute
   */

  public void setOperatingcompany(String operatingcompany) {

    this.operatingcompany = operatingcompany;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getAddress() address}.
   */
  public StringSearchConfigTo getAddressOption() {

    return this.addressOption;
  }

  /**
   * @param addressOption new value of {@link #getAddressOption()}.
   */
  public void setAddressOption(StringSearchConfigTo addressOption) {

    this.addressOption = addressOption;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getOperatingcompany() operatingcompany}.
   */
  public StringSearchConfigTo getOperatingcompanyOption() {

    return this.operatingcompanyOption;
  }

  /**
   * @param operatingcompanyOption new value of {@link #getOperatingcompanyOption()}.
   */
  public void setOperatingcompanyOption(StringSearchConfigTo operatingcompanyOption) {

    this.operatingcompanyOption = operatingcompanyOption;
  }

}
