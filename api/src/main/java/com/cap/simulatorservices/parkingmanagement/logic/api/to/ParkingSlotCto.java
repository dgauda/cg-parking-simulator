package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.devonfw.module.basic.common.api.to.AbstractCto;

/**
 * Composite transport object of ParkingSlot
 */
public class ParkingSlotCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ParkingSlotEto parkingSlot;

  private ParkingLotEto parkingLot;

  private ParkingSlotStatusEto status;

  public ParkingSlotEto getParkingSlot() {

    return parkingSlot;
  }

  public void setParkingSlot(ParkingSlotEto parkingSlot) {

    this.parkingSlot = parkingSlot;
  }

  public ParkingLotEto getParkingLot() {

    return parkingLot;
  }

  public void setParkingLot(ParkingLotEto parkingLot) {

    this.parkingLot = parkingLot;
  }

  public ParkingSlotStatusEto getStatus() {

    return status;
  }

  public void setStatus(ParkingSlotStatusEto status) {

    this.status = status;
  }

}
