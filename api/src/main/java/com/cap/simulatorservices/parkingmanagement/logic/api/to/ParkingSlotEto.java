package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.cap.simulatorservices.parkingmanagement.common.api.ParkingSlot;
import com.devonfw.module.basic.common.api.to.AbstractEto;

/**
 * Entity transport object of ParkingSlot
 */
public class ParkingSlotEto extends AbstractEto implements ParkingSlot {

  private static final long serialVersionUID = 1L;

  private Long parkingLotId;

  private Long slotNumber;

  private Long statusId;

  @Override
  public Long getParkingLotId() {

    return parkingLotId;
  }

  @Override
  public void setParkingLotId(Long parkingLotId) {

    this.parkingLotId = parkingLotId;
  }

  @Override
  public Long getSlotNumber() {

    return slotNumber;
  }

  @Override
  public void setSlotNumber(Long slotNumber) {

    this.slotNumber = slotNumber;
  }

  @Override
  public Long getStatusId() {

    return statusId;
  }

  @Override
  public void setStatusId(Long statusId) {

    this.statusId = statusId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();

    result = prime * result + ((this.parkingLotId == null) ? 0 : this.parkingLotId.hashCode());
    result = prime * result + ((this.slotNumber == null) ? 0 : this.slotNumber.hashCode());

    result = prime * result + ((this.statusId == null) ? 0 : this.statusId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    ParkingSlotEto other = (ParkingSlotEto) obj;

    if (this.parkingLotId == null) {
      if (other.parkingLotId != null) {
        return false;
      }
    } else if (!this.parkingLotId.equals(other.parkingLotId)) {
      return false;
    }
    if (this.slotNumber == null) {
      if (other.slotNumber != null) {
        return false;
      }
    } else if (!this.slotNumber.equals(other.slotNumber)) {
      return false;
    }

    if (this.statusId == null) {
      if (other.statusId != null) {
        return false;
      }
    } else if (!this.statusId.equals(other.statusId)) {
      return false;
    }
    return true;
  }
}
