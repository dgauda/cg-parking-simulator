package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractSearchCriteriaTo;

/**
 * {@link SearchCriteriaTo} to find instances of
 * {@link com.cap.simulatorservices.parkingmanagement.common.api.ParkingSlot}s.
 */
public class ParkingSlotSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Long parkingLotId;

  private Long slotNumber;

  private Long statusId;

  /**
   * getter for parkingLotId attribute
   * 
   * @return parkingLotId
   */

  public Long getParkingLotId() {

    return parkingLotId;
  }

  /**
   * @param parkingLot setter for parkingLot attribute
   */

  public void setParkingLotId(Long parkingLotId) {

    this.parkingLotId = parkingLotId;
  }

  /**
   * @return slotNumberId
   */

  public Long getSlotNumber() {

    return slotNumber;
  }

  /**
   * @param slotNumber setter for slotNumber attribute
   */

  public void setSlotNumber(Long slotNumber) {

    this.slotNumber = slotNumber;
  }

  /**
   * getter for statusId attribute
   * 
   * @return statusId
   */

  public Long getStatusId() {

    return statusId;
  }

  /**
   * @param status setter for status attribute
   */

  public void setStatusId(Long statusId) {

    this.statusId = statusId;
  }

}
