package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.devonfw.module.basic.common.api.to.AbstractCto;

/**
 * Composite transport object of Reservation
 */
public class ReservationCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ReservationEto reservation;

  private ParkingLotEto parkingLot;

  private ParkingSlotEto parkingSlot;

  public ReservationEto getReservation() {

    return reservation;
  }

  public void setReservation(ReservationEto reservation) {

    this.reservation = reservation;
  }

  public ParkingLotEto getParkingLot() {

    return parkingLot;
  }

  public void setParkingLot(ParkingLotEto parkingLot) {

    this.parkingLot = parkingLot;
  }

  public ParkingSlotEto getParkingSlot() {

    return parkingSlot;
  }

  public void setParkingSlot(ParkingSlotEto parkingSlot) {

    this.parkingSlot = parkingSlot;
  }

}
