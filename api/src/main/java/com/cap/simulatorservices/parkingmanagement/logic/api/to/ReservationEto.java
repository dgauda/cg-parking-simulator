package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import java.sql.Timestamp;

import com.cap.simulatorservices.parkingmanagement.common.api.Reservation;
import com.devonfw.module.basic.common.api.to.AbstractEto;

/**
 * Entity transport object of Reservation
 */
public class ReservationEto extends AbstractEto implements Reservation {

  private static final long serialVersionUID = 1L;

  private Long itineraryId;

  private Timestamp startTimestamp;

  private Timestamp endTimestamp;

  private Long parkingLotId;

  private Long parkingSlotId;

  @Override
  public Long getItineraryId() {

    return this.itineraryId;
  }

  @Override
  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

  @Override
  public Timestamp getStartTimestamp() {

    return this.startTimestamp;
  }

  @Override
  public void setStartTimestamp(Timestamp startTimestamp) {

    this.startTimestamp = startTimestamp;
  }

  @Override
  public Timestamp getEndTimestamp() {

    return this.endTimestamp;
  }

  @Override
  public void setEndTimestamp(Timestamp endTimestamp) {

    this.endTimestamp = endTimestamp;
  }

  @Override
  public Long getParkingLotId() {

    return this.parkingLotId;
  }

  @Override
  public void setParkingLotId(Long parkingLotId) {

    this.parkingLotId = parkingLotId;
  }

  @Override
  public Long getParkingSlotId() {

    return this.parkingSlotId;
  }

  @Override
  public void setParkingSlotId(Long parkingSlotId) {

    this.parkingSlotId = parkingSlotId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.itineraryId == null) ? 0 : this.itineraryId.hashCode());
    result = prime * result + ((this.startTimestamp == null) ? 0 : this.startTimestamp.hashCode());
    result = prime * result + ((this.endTimestamp == null) ? 0 : this.endTimestamp.hashCode());

    result = prime * result + ((this.parkingLotId == null) ? 0 : this.parkingLotId.hashCode());

    result = prime * result + ((this.parkingSlotId == null) ? 0 : this.parkingSlotId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    ReservationEto other = (ReservationEto) obj;
    if (this.itineraryId == null) {
      if (other.itineraryId != null) {
        return false;
      }
    } else if (!this.itineraryId.equals(other.itineraryId)) {
      return false;
    }
    if (this.startTimestamp == null) {
      if (other.startTimestamp != null) {
        return false;
      }
    } else if (!this.startTimestamp.equals(other.startTimestamp)) {
      return false;
    }
    if (this.endTimestamp == null) {
      if (other.endTimestamp != null) {
        return false;
      }
    } else if (!this.endTimestamp.equals(other.endTimestamp)) {
      return false;
    }

    if (this.parkingLotId == null) {
      if (other.parkingLotId != null) {
        return false;
      }
    } else if (!this.parkingLotId.equals(other.parkingLotId)) {
      return false;
    }

    if (this.parkingSlotId == null) {
      if (other.parkingSlotId != null) {
        return false;
      }
    } else if (!this.parkingSlotId.equals(other.parkingSlotId)) {
      return false;
    }
    return true;
  }
}
