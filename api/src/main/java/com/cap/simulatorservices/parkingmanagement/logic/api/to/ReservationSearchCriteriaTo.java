package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.to.AbstractSearchCriteriaTo;

/**
 * {@link SearchCriteriaTo} to find instances of
 * {@link com.cap.simulatorservices.parkingmanagement.common.api.Reservation}s.
 */
public class ReservationSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Long itineraryId;

  private Timestamp startTimestamp;

  private Timestamp endTimestamp;

  private Long parkingLotId;

  private Long parkingSlotId;

  /**
   * @return itineraryIdId
   */

  public Long getItineraryId() {

    return this.itineraryId;
  }

  /**
   * @param itineraryId setter for itineraryId attribute
   */

  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

  /**
   * @return startTimestampId
   */

  public Timestamp getStartTimestamp() {

    return this.startTimestamp;
  }

  /**
   * @param startTimestamp setter for startTimestamp attribute
   */

  public void setStartTimestamp(Timestamp startTimestamp) {

    this.startTimestamp = startTimestamp;
  }

  /**
   * @return endTimestampId
   */

  public Timestamp getEndTimestamp() {

    return this.endTimestamp;
  }

  /**
   * @param endTimestamp setter for endTimestamp attribute
   */

  public void setEndTimestamp(Timestamp endTimestamp) {

    this.endTimestamp = endTimestamp;
  }

  /**
   * getter for parkingLotId attribute
   *
   * @return parkingLotId
   */

  public Long getParkingLotId() {

    return this.parkingLotId;
  }

  /**
   * @param parkingLot setter for parkingLot attribute
   */

  public void setParkingLotId(Long parkingLotId) {

    this.parkingLotId = parkingLotId;
  }

  /**
   * getter for parkingSlotId attribute
   *
   * @return parkingSlotId
   */

  public Long getParkingSlotId() {

    return this.parkingSlotId;
  }

  /**
   * @param parkingSlot setter for parkingSlot attribute
   */

  public void setParkingSlotId(Long parkingSlotId) {

    this.parkingSlotId = parkingSlotId;
  }

}
