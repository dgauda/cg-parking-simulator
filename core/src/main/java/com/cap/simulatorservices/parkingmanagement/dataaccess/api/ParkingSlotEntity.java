package com.cap.simulatorservices.parkingmanagement.dataaccess.api;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;
import com.cap.simulatorservices.parkingmanagement.common.api.ParkingSlot;

/**
 * @author dgauda
 */
@Entity
@Table(name = "ParkingSlot")
public class ParkingSlotEntity extends ApplicationPersistenceEntity implements ParkingSlot {

  private ParkingLotEntity parkingLot;

  private Long slotNumber;

  private ParkingSlotStatusEntity status;

  private static final long serialVersionUID = 1L;

  /**
   * @return slotNumber
   */
  public Long getSlotNumber() {

    return this.slotNumber;
  }

  /**
   * @return type
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idParkingSlotStatus")
  public ParkingSlotStatusEntity getStatus() {

    return this.status;
  }

  /**
   * @param status new value of {@link #getstatus}.
   */
  public void setStatus(ParkingSlotStatusEntity status) {

    this.status = status;
  }

  /**
   * @param slotNumber new value of {@link #getslotNumber}.
   */
  public void setSlotNumber(Long slotNumber) {

    this.slotNumber = slotNumber;
  }

  /**
   * @return parkingLotDetails
   */
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idParkingLot")
  public ParkingLotEntity getParkingLot() {

    return this.parkingLot;
  }

  /**
   * @param parkingLotDetails new value of {@link #getparkingLotDetails}.
   */
  public void setParkingLot(ParkingLotEntity parkingLot) {

    this.parkingLot = parkingLot;
  }

  @Override
  @Transient
  public Long getParkingLotId() {

    if (this.parkingLot == null) {
      return null;
    }
    return this.parkingLot.getId();
  }

  @Override
  public void setParkingLotId(Long parkingLotId) {

    if (parkingLotId == null) {
      this.parkingLot = null;
    } else {
      ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
      parkingLotEntity.setId(parkingLotId);
      this.parkingLot = parkingLotEntity;
    }
  }

  @Override
  @Transient
  public Long getStatusId() {

    if (this.status == null) {
      return null;
    }
    return this.status.getId();
  }

  @Override
  public void setStatusId(Long statusId) {

    if (statusId == null) {
      this.status = null;
    } else {
      ParkingSlotStatusEntity parkingSlotStatusEntity = new ParkingSlotStatusEntity();
      parkingSlotStatusEntity.setId(statusId);
      this.status = parkingSlotStatusEntity;
    }
  }

}
