package com.cap.simulatorservices.parkingmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingLotEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link ParkingLotEntity}
 */
public interface ParkingLotRepository extends DefaultRepository<ParkingLotEntity> {

  /**
   * @param criteria the {@link ParkingLotSearchCriteriaTo} with the criteria to search.
   * @param pageRequest {@link Pageable} implementation used to set page properties like page size
   * @return the {@link Page} of the {@link ParkingLotEntity} objects that matched the search.
   */
  default Page<ParkingLotEntity> findByCriteria(ParkingLotSearchCriteriaTo criteria) {

    ParkingLotEntity alias = newDslAlias();
    JPAQuery<ParkingLotEntity> query = newDslQuery(alias);

    String address = criteria.getAddress();
    if (address != null && !address.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getAddress()), address, criteria.getAddressOption());
    }
    Double latitude = criteria.getLatitude();
    if (latitude != null) {
      query.where($(alias.getLatitude()).eq(latitude));
    }
    Double longitude = criteria.getLongitude();
    if (longitude != null) {
      query.where($(alias.getLongitude()).eq(longitude));
    }
    Long totalNoOfSlots = criteria.getTotalNoOfSlots();
    if (totalNoOfSlots != null) {
      query.where($(alias.getTotalNoOfSlots()).eq(totalNoOfSlots));
    }
    Long availableNoOfSlots = criteria.getAvailableNoOfSlots();
    if (availableNoOfSlots != null) {
      query.where($(alias.getAvailableNoOfSlots()).eq(availableNoOfSlots));
    }
    String operatingcompany = criteria.getOperatingcompany();
    if (operatingcompany != null && !operatingcompany.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getOperatingcompany()), operatingcompany,
          criteria.getOperatingcompanyOption());
    }
    addOrderBy(query, alias, criteria.getPageable().getSort());

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   * 
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<ParkingLotEntity> query, ParkingLotEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "address":
            if (next.isAscending()) {
              query.orderBy($(alias.getAddress()).asc());
            } else {
              query.orderBy($(alias.getAddress()).desc());
            }
            break;
          case "latitude":
            if (next.isAscending()) {
              query.orderBy($(alias.getLatitude()).asc());
            } else {
              query.orderBy($(alias.getLatitude()).desc());
            }
            break;
          case "longitude":
            if (next.isAscending()) {
              query.orderBy($(alias.getLongitude()).asc());
            } else {
              query.orderBy($(alias.getLongitude()).desc());
            }
            break;
          case "totalNoOfSlots":
            if (next.isAscending()) {
              query.orderBy($(alias.getTotalNoOfSlots()).asc());
            } else {
              query.orderBy($(alias.getTotalNoOfSlots()).desc());
            }
            break;
          case "availableNoOfSlots":
            if (next.isAscending()) {
              query.orderBy($(alias.getAvailableNoOfSlots()).asc());
            } else {
              query.orderBy($(alias.getAvailableNoOfSlots()).desc());
            }
            break;
          case "operatingcompany":
            if (next.isAscending()) {
              query.orderBy($(alias.getOperatingcompany()).asc());
            } else {
              query.orderBy($(alias.getOperatingcompany()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}