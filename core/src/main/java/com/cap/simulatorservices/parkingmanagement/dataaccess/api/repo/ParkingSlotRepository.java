package com.cap.simulatorservices.parkingmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link ParkingSlotEntity}
 */
public interface ParkingSlotRepository extends DefaultRepository<ParkingSlotEntity> {

  /**
   * @param criteria the {@link ParkingSlotSearchCriteriaTo} with the criteria to search.
   * @param pageRequest {@link Pageable} implementation used to set page properties like page size
   * @return the {@link Page} of the {@link ParkingSlotEntity} objects that matched the search.
   */
  default Page<ParkingSlotEntity> findByCriteria(ParkingSlotSearchCriteriaTo criteria) {

    ParkingSlotEntity alias = newDslAlias();
    JPAQuery<ParkingSlotEntity> query = newDslQuery(alias);

    Long parkingLot = criteria.getParkingLotId();
    if (parkingLot != null) {
      query.where($(alias.getParkingLot().getId()).eq(parkingLot));
    }
    Long slotNumber = criteria.getSlotNumber();
    if (slotNumber != null) {
      query.where($(alias.getSlotNumber()).eq(slotNumber));
    }
    Long status = criteria.getStatusId();
    if (status != null) {
      query.where($(alias.getStatus().getId()).eq(status));
    }
    addOrderBy(query, alias, criteria.getPageable().getSort());

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   * 
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<ParkingSlotEntity> query, ParkingSlotEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "parkingLot":
            if (next.isAscending()) {
              query.orderBy($(alias.getParkingLot().getId()).asc());
            } else {
              query.orderBy($(alias.getParkingLot().getId()).desc());
            }
            break;
          case "slotNumber":
            if (next.isAscending()) {
              query.orderBy($(alias.getSlotNumber()).asc());
            } else {
              query.orderBy($(alias.getSlotNumber()).desc());
            }
            break;
          case "status":
            if (next.isAscending()) {
              query.orderBy($(alias.getStatus().getId()).asc());
            } else {
              query.orderBy($(alias.getStatus().getId()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}