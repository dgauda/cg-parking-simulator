package com.cap.simulatorservices.parkingmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.sql.Timestamp;
import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ReservationEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link ReservationEntity}
 */
public interface ReservationRepository extends DefaultRepository<ReservationEntity> {

  /**
   * @param criteria the {@link ReservationSearchCriteriaTo} with the criteria to search.
   * @param pageRequest {@link Pageable} implementation used to set page properties like page size
   * @return the {@link Page} of the {@link ReservationEntity} objects that matched the search.
   */
  default Page<ReservationEntity> findByCriteria(ReservationSearchCriteriaTo criteria) {

    ReservationEntity alias = newDslAlias();
    JPAQuery<ReservationEntity> query = newDslQuery(alias);

    Long itineraryId = criteria.getItineraryId();
    if (itineraryId != null) {
      query.where($(alias.getItineraryId()).eq(itineraryId));
    }
    Timestamp startTimestamp = criteria.getStartTimestamp();
    if (startTimestamp != null) {
      query.where($(alias.getStartTimestamp()).eq(startTimestamp));
    }
    Timestamp endTimestamp = criteria.getEndTimestamp();
    if (endTimestamp != null) {
      query.where($(alias.getEndTimestamp()).eq(endTimestamp));
    }
    Long parkingLot = criteria.getParkingLotId();
    if (parkingLot != null) {
      query.where($(alias.getParkingLot().getId()).eq(parkingLot));
    }
    Long parkingSlot = criteria.getParkingSlotId();
    if (parkingSlot != null) {
      query.where($(alias.getParkingSlot().getId()).eq(parkingSlot));
    }
    addOrderBy(query, alias, criteria.getPageable().getSort());

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   *
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<ReservationEntity> query, ReservationEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "itineraryId":
            if (next.isAscending()) {
              query.orderBy($(alias.getItineraryId()).asc());
            } else {
              query.orderBy($(alias.getItineraryId()).desc());
            }
            break;
          case "startTimestamp":
            if (next.isAscending()) {
              query.orderBy($(alias.getStartTimestamp()).asc());
            } else {
              query.orderBy($(alias.getStartTimestamp()).desc());
            }
            break;
          case "endTimestamp":
            if (next.isAscending()) {
              query.orderBy($(alias.getEndTimestamp()).asc());
            } else {
              query.orderBy($(alias.getEndTimestamp()).desc());
            }
            break;
          case "parkingLot":
            if (next.isAscending()) {
              query.orderBy($(alias.getParkingLot().getId()).asc());
            } else {
              query.orderBy($(alias.getParkingLot().getId()).desc());
            }
            break;
          case "parkingSlot":
            if (next.isAscending()) {
              query.orderBy($(alias.getParkingSlot().getId()).asc());
            } else {
              query.orderBy($(alias.getParkingSlot().getId()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}