package com.cap.simulatorservices.parkingmanagement.logic.api;

import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingLot;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingSlot;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingSlotStatus;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindReservation;

/**
 * Interface for Parkingmanagement component.
 */
public interface Parkingmanagement
    extends UcFindParkingSlotStatus, UcFindParkingLot, UcFindParkingSlot, UcFindReservation {

}
