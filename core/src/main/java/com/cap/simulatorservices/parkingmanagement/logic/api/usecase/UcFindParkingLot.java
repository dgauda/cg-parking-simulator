package com.cap.simulatorservices.parkingmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;

public interface UcFindParkingLot {

  /**
   * Returns a composite ParkingLot by its id 'id'.
   *
   * @param id The id 'id' of the ParkingLot.
   * @return The {@link ParkingLotCto} with id 'id'
   */
  ParkingLotCto findParkingLotCto(long id);

  /**
   * Returns a paginated list of composite ParkingLots matching the search criteria.
   *
   * @param criteria the {@link ParkingLotSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingLotCto}s.
   */
  Page<ParkingLotCto> findParkingLotCtos(ParkingLotSearchCriteriaTo criteria);

}
