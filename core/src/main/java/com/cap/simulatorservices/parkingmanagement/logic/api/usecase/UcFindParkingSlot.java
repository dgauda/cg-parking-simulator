package com.cap.simulatorservices.parkingmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;

public interface UcFindParkingSlot {

  /**
   * Returns a composite ParkingSlot by its id 'id'.
   *
   * @param id The id 'id' of the ParkingSlot.
   * @return The {@link ParkingSlotCto} with id 'id'
   */
  ParkingSlotCto findParkingSlotCto(long id);

  /**
   * Returns a paginated list of composite ParkingSlots matching the search criteria.
   *
   * @param criteria the {@link ParkingSlotSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingSlotCto}s.
   */
  Page<ParkingSlotCto> findParkingSlotCtos(ParkingSlotSearchCriteriaTo criteria);

}
