package com.cap.simulatorservices.parkingmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;

public interface UcFindParkingSlotStatus {

  /**
   * Returns a composite ParkingSlotStatus by its id 'id'.
   *
   * @param id The id 'id' of the ParkingSlotStatus.
   * @return The {@link ParkingSlotStatusCto} with id 'id'
   */
  ParkingSlotStatusCto findParkingSlotStatusCto(long id);

  /**
   * Returns a paginated list of composite ParkingSlotStatuss matching the search criteria.
   *
   * @param criteria the {@link ParkingSlotStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingSlotStatusCto}s.
   */
  Page<ParkingSlotStatusCto> findParkingSlotStatusCtos(ParkingSlotStatusSearchCriteriaTo criteria);

}
