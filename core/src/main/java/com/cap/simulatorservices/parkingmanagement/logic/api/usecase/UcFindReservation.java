package com.cap.simulatorservices.parkingmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;

public interface UcFindReservation {

  /**
   * Returns a composite Reservation by its id 'id'.
   *
   * @param id The id 'id' of the Reservation.
   * @return The {@link ReservationCto} with id 'id'
   */
  ReservationCto findReservationCto(long id);

  /**
   * Returns a paginated list of composite Reservations matching the search criteria.
   *
   * @param criteria the {@link ReservationSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ReservationCto}s.
   */
  Page<ReservationCto> findReservationCtos(ReservationSearchCriteriaTo criteria);

}
