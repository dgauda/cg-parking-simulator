package com.cap.simulatorservices.parkingmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.cap.simulatorservices.general.logic.base.AbstractUc;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.repo.ParkingLotRepository;

/**
 * Abstract use case for ParkingLots, which provides access to the commonly necessary data access objects.
 */
public class AbstractParkingLotUc extends AbstractUc {

  /** @see #getParkingLotRepository() */
  @Inject
  private ParkingLotRepository parkingLotRepository;

  /**
   * Returns the field 'parkingLotRepository'.
   * 
   * @return the {@link ParkingLotRepository} instance.
   */
  public ParkingLotRepository getParkingLotRepository() {

    return this.parkingLotRepository;
  }

}
