package com.cap.simulatorservices.parkingmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.cap.simulatorservices.general.logic.base.AbstractUc;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.repo.ParkingSlotStatusRepository;

/**
 * Abstract use case for ParkingSlotStatuss, which provides access to the commonly necessary data access objects.
 */
public class AbstractParkingSlotStatusUc extends AbstractUc {

  /** @see #getParkingSlotStatusRepository() */
  @Inject
  private ParkingSlotStatusRepository parkingSlotStatusRepository;

  /**
   * Returns the field 'parkingSlotStatusRepository'.
   * 
   * @return the {@link ParkingSlotStatusRepository} instance.
   */
  public ParkingSlotStatusRepository getParkingSlotStatusRepository() {

    return this.parkingSlotStatusRepository;
  }

}
