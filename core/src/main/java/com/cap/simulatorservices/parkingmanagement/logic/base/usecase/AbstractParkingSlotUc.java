package com.cap.simulatorservices.parkingmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.cap.simulatorservices.general.logic.base.AbstractUc;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.repo.ParkingSlotRepository;

/**
 * Abstract use case for ParkingSlots, which provides access to the commonly necessary data access objects.
 */
public class AbstractParkingSlotUc extends AbstractUc {

  /** @see #getParkingSlotRepository() */
  @Inject
  private ParkingSlotRepository parkingSlotRepository;

  /**
   * Returns the field 'parkingSlotRepository'.
   * 
   * @return the {@link ParkingSlotRepository} instance.
   */
  public ParkingSlotRepository getParkingSlotRepository() {

    return this.parkingSlotRepository;
  }

}
