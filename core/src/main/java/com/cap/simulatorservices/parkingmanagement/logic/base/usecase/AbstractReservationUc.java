package com.cap.simulatorservices.parkingmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.cap.simulatorservices.general.logic.base.AbstractUc;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.repo.ReservationRepository;

/**
 * Abstract use case for Reservations, which provides access to the commonly necessary data access objects.
 */
public class AbstractReservationUc extends AbstractUc {

  /** @see #getReservationRepository() */
  @Inject
  private ReservationRepository reservationRepository;

  /**
   * Returns the field 'reservationRepository'.
   * 
   * @return the {@link ReservationRepository} instance.
   */
  public ReservationRepository getReservationRepository() {

    return this.reservationRepository;
  }

}
