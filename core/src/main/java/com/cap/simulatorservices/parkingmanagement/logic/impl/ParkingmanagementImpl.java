package com.cap.simulatorservices.parkingmanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.general.logic.base.AbstractComponentFacade;
import com.cap.simulatorservices.parkingmanagement.logic.api.Parkingmanagement;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingLot;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingSlot;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingSlotStatus;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindReservation;

/**
 * Implementation of component interface of parkingmanagement
 */
@Named
public class ParkingmanagementImpl extends AbstractComponentFacade implements Parkingmanagement {

  @Inject
  private UcFindParkingSlotStatus ucFindParkingSlotStatus;

  @Inject
  private UcFindParkingLot ucFindParkingLot;

  @Inject
  private UcFindParkingSlot ucFindParkingSlot;

  @Inject
  private UcFindReservation ucFindReservation;

  @Override
  public ParkingSlotStatusCto findParkingSlotStatusCto(long id) {

    return ucFindParkingSlotStatus.findParkingSlotStatusCto(id);
  }

  @Override
  public Page<ParkingSlotStatusCto> findParkingSlotStatusCtos(ParkingSlotStatusSearchCriteriaTo criteria) {

    return ucFindParkingSlotStatus.findParkingSlotStatusCtos(criteria);
  }

  @Override
  public ParkingLotCto findParkingLotCto(long id) {

    return ucFindParkingLot.findParkingLotCto(id);
  }

  @Override
  public Page<ParkingLotCto> findParkingLotCtos(ParkingLotSearchCriteriaTo criteria) {

    return ucFindParkingLot.findParkingLotCtos(criteria);
  }

  @Override
  public ParkingSlotCto findParkingSlotCto(long id) {

    return ucFindParkingSlot.findParkingSlotCto(id);
  }

  @Override
  public Page<ParkingSlotCto> findParkingSlotCtos(ParkingSlotSearchCriteriaTo criteria) {

    return ucFindParkingSlot.findParkingSlotCtos(criteria);
  }

  @Override
  public ReservationCto findReservationCto(long id) {

    return ucFindReservation.findReservationCto(id);
  }

  @Override
  public Page<ReservationCto> findReservationCtos(ReservationSearchCriteriaTo criteria) {

    return ucFindReservation.findReservationCtos(criteria);
  }

}
