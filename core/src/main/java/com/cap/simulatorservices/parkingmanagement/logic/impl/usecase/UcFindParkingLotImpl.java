package com.cap.simulatorservices.parkingmanagement.logic.impl.usecase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingLotEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingLot;
import com.cap.simulatorservices.parkingmanagement.logic.base.usecase.AbstractParkingLotUc;

/**
 * Use case implementation for searching, filtering and getting ParkingLots
 */
@Named
@Validated
@Transactional
public class UcFindParkingLotImpl extends AbstractParkingLotUc implements UcFindParkingLot {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindParkingLotImpl.class);

  @Override
  public ParkingLotCto findParkingLotCto(long id) {

    LOG.debug("Get ParkingLotCto with id {} from database.", id);
    ParkingLotEntity entity = getParkingLotRepository().find(id);
    ParkingLotCto cto = new ParkingLotCto();
    cto.setParkingLot(getBeanMapper().map(entity, ParkingLotEto.class));
    // cto.setParkingSlots(getBeanMapper().mapList(entity.getParkingSlots(), ParkingSlotEto.class));

    return cto;
  }

  @Override
  public Page<ParkingLotCto> findParkingLotCtos(ParkingLotSearchCriteriaTo criteria) {

    Page<ParkingLotEntity> parkinglots = getParkingLotRepository().findByCriteria(criteria);
    List<ParkingLotCto> ctos = new ArrayList<>();
    for (ParkingLotEntity entity : parkinglots.getContent()) {
      ParkingLotCto cto = new ParkingLotCto();
      cto.setParkingLot(getBeanMapper().map(entity, ParkingLotEto.class));
      // cto.setParkingSlots(getBeanMapper().mapList(entity.getParkingSlots(), ParkingSlotEto.class));
      ctos.add(cto);
    }
    Pageable pagResultTo = PageRequest.of(criteria.getPageable().getPageNumber(), criteria.getPageable().getPageSize());

    return new PageImpl<>(ctos, pagResultTo, parkinglots.getTotalElements());
  }
}
