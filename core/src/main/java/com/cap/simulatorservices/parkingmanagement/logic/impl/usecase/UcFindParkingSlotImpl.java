package com.cap.simulatorservices.parkingmanagement.logic.impl.usecase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingSlot;
import com.cap.simulatorservices.parkingmanagement.logic.base.usecase.AbstractParkingSlotUc;

/**
 * Use case implementation for searching, filtering and getting ParkingSlots
 */
@Named
@Validated
@Transactional
public class UcFindParkingSlotImpl extends AbstractParkingSlotUc implements UcFindParkingSlot {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindParkingSlotImpl.class);

  @Override
  public ParkingSlotCto findParkingSlotCto(long id) {

    LOG.debug("Get ParkingSlotCto with id {} from database.", id);
    ParkingSlotEntity entity = getParkingSlotRepository().find(id);
    ParkingSlotCto cto = new ParkingSlotCto();
    cto.setParkingSlot(getBeanMapper().map(entity, ParkingSlotEto.class));
    cto.setParkingLot(getBeanMapper().map(entity.getParkingLot(), ParkingLotEto.class));
    cto.setStatus(getBeanMapper().map(entity.getStatus(), ParkingSlotStatusEto.class));

    return cto;
  }

  @Override
  public Page<ParkingSlotCto> findParkingSlotCtos(ParkingSlotSearchCriteriaTo criteria) {

    Page<ParkingSlotEntity> parkingslots = getParkingSlotRepository().findByCriteria(criteria);
    List<ParkingSlotCto> ctos = new ArrayList<>();
    for (ParkingSlotEntity entity : parkingslots.getContent()) {
      ParkingSlotCto cto = new ParkingSlotCto();
      cto.setParkingSlot(getBeanMapper().map(entity, ParkingSlotEto.class));
      cto.setParkingLot(getBeanMapper().map(entity.getParkingLot(), ParkingLotEto.class));
      cto.setStatus(getBeanMapper().map(entity.getStatus(), ParkingSlotStatusEto.class));
      ctos.add(cto);
    }
    Pageable pagResultTo = PageRequest.of(criteria.getPageable().getPageNumber(), criteria.getPageable().getPageSize());

    return new PageImpl<>(ctos, pagResultTo, parkingslots.getTotalElements());
  }
}
