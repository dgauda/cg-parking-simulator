package com.cap.simulatorservices.parkingmanagement.logic.impl.usecase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotStatusEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindParkingSlotStatus;
import com.cap.simulatorservices.parkingmanagement.logic.base.usecase.AbstractParkingSlotStatusUc;

/**
 * Use case implementation for searching, filtering and getting ParkingSlotStatuss
 */
@Named
@Validated
@Transactional
public class UcFindParkingSlotStatusImpl extends AbstractParkingSlotStatusUc implements UcFindParkingSlotStatus {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindParkingSlotStatusImpl.class);

  @Override
  public ParkingSlotStatusCto findParkingSlotStatusCto(long id) {

    LOG.debug("Get ParkingSlotStatusCto with id {} from database.", id);
    ParkingSlotStatusEntity entity = getParkingSlotStatusRepository().find(id);
    ParkingSlotStatusCto cto = new ParkingSlotStatusCto();
    cto.setParkingSlotStatus(getBeanMapper().map(entity, ParkingSlotStatusEto.class));

    return cto;
  }

  @Override
  public Page<ParkingSlotStatusCto> findParkingSlotStatusCtos(ParkingSlotStatusSearchCriteriaTo criteria) {

    Page<ParkingSlotStatusEntity> parkingslotstatuss = getParkingSlotStatusRepository().findByCriteria(criteria);
    List<ParkingSlotStatusCto> ctos = new ArrayList<>();
    for (ParkingSlotStatusEntity entity : parkingslotstatuss.getContent()) {
      ParkingSlotStatusCto cto = new ParkingSlotStatusCto();
      cto.setParkingSlotStatus(getBeanMapper().map(entity, ParkingSlotStatusEto.class));
      ctos.add(cto);
    }
    Pageable pagResultTo = PageRequest.of(criteria.getPageable().getPageNumber(), criteria.getPageable().getPageSize());

    return new PageImpl<>(ctos, pagResultTo, parkingslotstatuss.getTotalElements());
  }
}
