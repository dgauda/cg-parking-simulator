package com.cap.simulatorservices.parkingmanagement.logic.impl.usecase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ReservationEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.usecase.UcFindReservation;
import com.cap.simulatorservices.parkingmanagement.logic.base.usecase.AbstractReservationUc;

/**
 * Use case implementation for searching, filtering and getting Reservations
 */
@Named
@Validated
@Transactional
public class UcFindReservationImpl extends AbstractReservationUc implements UcFindReservation {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindReservationImpl.class);

  @Override
  public ReservationCto findReservationCto(long id) {

    LOG.debug("Get ReservationCto with id {} from database.", id);
    ReservationEntity entity = getReservationRepository().find(id);
    ReservationCto cto = new ReservationCto();
    cto.setReservation(getBeanMapper().map(entity, ReservationEto.class));
    cto.setParkingLot(getBeanMapper().map(entity.getParkingLot(), ParkingLotEto.class));
    cto.setParkingSlot(getBeanMapper().map(entity.getParkingSlot(), ParkingSlotEto.class));

    return cto;
  }

  @Override
  public Page<ReservationCto> findReservationCtos(ReservationSearchCriteriaTo criteria) {

    Page<ReservationEntity> reservations = getReservationRepository().findByCriteria(criteria);
    List<ReservationCto> ctos = new ArrayList<>();
    for (ReservationEntity entity : reservations.getContent()) {
      ReservationCto cto = new ReservationCto();
      cto.setReservation(getBeanMapper().map(entity, ReservationEto.class));
      cto.setParkingLot(getBeanMapper().map(entity.getParkingLot(), ParkingLotEto.class));
      cto.setParkingSlot(getBeanMapper().map(entity.getParkingSlot(), ParkingSlotEto.class));
      ctos.add(cto);
    }
    Pageable pagResultTo = PageRequest.of(criteria.getPageable().getPageNumber(), criteria.getPageable().getPageSize());

    return new PageImpl<>(ctos, pagResultTo, reservations.getTotalElements());
  }
}
