package com.cap.simulatorservices.parkingmanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.parkingmanagement.logic.api.Parkingmanagement;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Parkingmanagement}.
 */
@Path("/parkingmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ParkingmanagementRestService {

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotStatusCto}.
   *
   * @param id the ID of the {@link ParkingSlotStatusCto}
   * @return the {@link ParkingSlotStatusCto}
   */
  @GET
  @Path("/parkingslotstatus/cto/{id}/")
  public ParkingSlotStatusCto getParkingSlotStatusCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotStatusCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkingslotstatuss.
   * @return the {@link Page list} of matching {@link ParkingSlotStatusCto}s.
   */
  @Path("/parkingslotstatus/cto/search")
  @POST
  public Page<ParkingSlotStatusCto> findParkingSlotStatusCtos(ParkingSlotStatusSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findParkingLotCto}.
   *
   * @param id the ID of the {@link ParkingLotCto}
   * @return the {@link ParkingLotCto}
   */
  @GET
  @Path("/parkinglot/cto/{id}/")
  public ParkingLotCto getParkingLotCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingLotCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkinglots.
   * @return the {@link Page list} of matching {@link ParkingLotCto}s.
   */
  @Path("/parkinglot/cto/search")
  @POST
  public Page<ParkingLotCto> findParkingLotCtos(ParkingLotSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotCto}.
   *
   * @param id the ID of the {@link ParkingSlotCto}
   * @return the {@link ParkingSlotCto}
   */
  @GET
  @Path("/parkingslot/cto/{id}/")
  public ParkingSlotCto getParkingSlotCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkingslots.
   * @return the {@link Page list} of matching {@link ParkingSlotCto}s.
   */
  @Path("/parkingslot/cto/search")
  @POST
  public Page<ParkingSlotCto> findParkingSlotCtos(ParkingSlotSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findReservationCto}.
   *
   * @param id the ID of the {@link ReservationCto}
   * @return the {@link ReservationCto}
   */
  @GET
  @Path("/reservation/cto/{id}/")
  public ReservationCto getReservationCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findReservationCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding reservations.
   * @return the {@link Page list} of matching {@link ReservationCto}s.
   */
  @Path("/reservation/cto/search")
  @POST
  public Page<ReservationCto> findReservationCtos(ReservationSearchCriteriaTo searchCriteriaTo);

}
