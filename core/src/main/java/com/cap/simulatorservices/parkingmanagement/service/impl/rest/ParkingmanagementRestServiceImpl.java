package com.cap.simulatorservices.parkingmanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.parkingmanagement.logic.api.Parkingmanagement;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.service.api.rest.ParkingmanagementRestService;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Parkingmanagement}.
 */
@Named("ParkingmanagementRestService")
public class ParkingmanagementRestServiceImpl implements ParkingmanagementRestService {

  @Inject
  private Parkingmanagement parkingmanagement;

  @Override
  public ParkingSlotStatusCto getParkingSlotStatusCto(long id) {

    return this.parkingmanagement.findParkingSlotStatusCto(id);
  }

  @Override
  public Page<ParkingSlotStatusCto> findParkingSlotStatusCtos(ParkingSlotStatusSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingSlotStatusCtos(searchCriteriaTo);
  }

  @Override
  public ParkingLotCto getParkingLotCto(long id) {

    return this.parkingmanagement.findParkingLotCto(id);
  }

  @Override
  public Page<ParkingLotCto> findParkingLotCtos(ParkingLotSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingLotCtos(searchCriteriaTo);
  }

  @Override
  public ParkingSlotCto getParkingSlotCto(long id) {

    return this.parkingmanagement.findParkingSlotCto(id);
  }

  @Override
  public Page<ParkingSlotCto> findParkingSlotCtos(ParkingSlotSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingSlotCtos(searchCriteriaTo);
  }

  @Override
  public ReservationCto getReservationCto(long id) {

    return this.parkingmanagement.findReservationCto(id);
  }

  @Override
  public Page<ReservationCto> findReservationCtos(ReservationSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findReservationCtos(searchCriteriaTo);
  }

}
