INSERT INTO ParkingLot (id, modificationCounter, address, latitude, longitude, totalNoOfSlots, availableNoOfSlots, operatingcompany) VALUES (1, 0, 'WhiteField Parking Lot, Bangalore', 9.1234567, 3.1234567, 10, 9, 'WhiteField Parking Lot Company');

Insert INTO ParkingSlotStatus (id, modificationCounter, name , desc) values (1, 0, 'Open', 'Parking Slot Open');
Insert INTO ParkingSlotStatus (id, modificationCounter, name , desc) values (2, 0, 'Occupied', 'Parking Slot Occupied');

INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (1, 0, 1, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (2, 0, 2, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (3, 0, 3, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (4, 0, 4, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (5, 0, 5, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (6, 0, 6, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (7, 0, 7, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (8, 0, 8, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (9, 0, 9, 1, 1);
INSERT INTO ParkingSlot (id, modificationCounter, slotNumber, idParkingLot, idParkingSlotStatus) VALUES (10, 0, 10, 1, 1);

INSERT INTO Reservation (id, modificationCounter, idParkingLot, idParkingSlot, itineraryId, startTimestamp, endTimestamp) VALUES (1, 0, 1, 3, 123, CURRENT_TIMESTAMP + (60 * 60 * 24 * 5), CURRENT_TIMESTAMP + (60 * 60 * 24 * 5));